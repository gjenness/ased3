### ASED3

This project implements a Grimme D3 Calculator for ASE.

Usage:

```
from ased3 import D3

calc = D3(
    xc='PBE',       # Which exchange-correlation functional to use
    bj=True,        # Whether to use Becke-Johnson damping, instead of zero-damping
    threebody=True, # Whether to include the Axilrod-Teller-Muto three-body terms
    calculator=dft, # A DFT calculator instance, e.g. VASP or GPAW. Optional.
    )

my_molecule.set_calculator(calc)

energy = my_molecule.get_potential_energy()
forces = my_molecule.get_forces()
stress = my_molecule.get_stress()
```

If you use ASED3 for work used in a publication, please cite the following papers by the authors of this method:

For D3 and D3(ABC):
Stefan Grimme, Jens Antony, Stephan Ehrlich, and Helge Krieg. J. Chem. Phys. 132, 154104 (2010); DOI:10.1063/1.3382344

For D3BJ and D3BJ(ABC):
Stefan Grimme, Stephan Ehrlich and Lars Goerigk. J. Comput. Chem. 32, 1456 (2011); DOI:10.1002/jcc.21759
